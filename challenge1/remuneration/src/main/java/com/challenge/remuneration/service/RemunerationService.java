package com.challenge.remuneration.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public interface RemunerationService {

     ArrayList<Map<String, String>> getRemunerationDataBy(Map<String,String> allRequestParams) throws IOException ;
}
