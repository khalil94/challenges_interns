package com.challenge.remuneration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class RemunerationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RemunerationApplication.class, args);
	}

}
